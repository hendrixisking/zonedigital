# Zonedigital - Interview task

This is the solution of the interview task for Zone Digital.

Two folders are exist:

 1. **api_service**: this is a service providing simple data. It's a really simple solution that uses PHP arrays to store the original data and after reading the parameter the belonging coordinate and city name will be given back as JSON structure.
 2. **endpoint**: the script reads a publis API and displays it in UI. With a dropdown user can select a city to see it's forecast.
**Last code update:  23/04/2018**

## Demo
Click [here](http://domainforssl.hu/interviewtask/zonedigital/endpoint/) to see my solution.




---
@author : Robert Koteles
