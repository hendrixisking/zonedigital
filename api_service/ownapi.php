<?php
session_start();

/*********************************
*	If data comes from database
*	here should be connecting to it. 
*********************************/

function myGet( $_variable ){ 
	if(get_magic_quotes_gpc()){
		//echo "Magic quotes are enabled";
		return trim($_GET[$_variable]);
	}else{
		//echo "Magic quotes are disabled";
		return trim(addslashes($_GET[$_variable]));
	}
}

// get callingparameter
$city = myGet('city');

/*********************************************/	
$output = array();


// in this example the data comes from php array
$cities = array (
  'london' => 
  array (
  	'name' => 'London',
    'latitude' => '51.507194',
    'longitude' => '-0.137311'
  ),
  'paris' => 
  array (
  	'name' => 'Paris',
    'latitude' => '48.856878',
    'longitude' => '2.348987'
  ),
  'newyork' => 
  array (
  	'name' => 'New York',
    'latitude' => '40.686658',
    'longitude' => '-73.970547'
  ),
  'singapore' => 
  array (
    'name' => 'Singapore',
    'latitude' => '1.346260',
    'longitude' => '103.847672'
  ),
  'sydney' => 
  array (
    'name' => 'Sydney',
    'latitude' => '-33.830817',
    'longitude' => '151.212088'
  )

);

if( !$city || $cities[ $city ] ){ 
		$output['resp'] = $cities[ $city ];
} else {
	$output['resp'] = 'wrong parameter';
}


echo json_encode( $output );
?>